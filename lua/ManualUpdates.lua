-- fix: exclude git repos
------------------
function BLTUpdate:IsGitRepo()
	if JimHUD:getSetting({ "Fixes", "EXCLUDE_GIT_REPOS" }, true) then
		local git_dir = Application:nice_path(self:GetInstallDirectory() .. "/" .. self:GetInstallFolder() .. "/.git", true)
		return SystemFS:exists(git_dir)
	else
		return false
	end
end

-- fix: show none_required notification if there are no updateable mods
------------------
function BLTModManager:_RunAutoCheckForUpdates()
	if BLT.Mods._updates_notification then
		BLT.Notifications:remove_notification(BLT.Mods._updates_notification)
		BLT.Mods._updates_notification = nil
	end

	-- Start checking all enabled mods for updates
	local count = 0
	for _, mod in ipairs(self:Mods()) do
		for _, update in ipairs(mod:GetUpdates()) do
			if update:IsEnabled() and not update:IsGitRepo() then
				if not BLT.Mods._updates_notification then
					-- Place a notification that we're checking for autoupdates
					local icon, rect = tweak_data.hud_icons:get_icon_data("csb_pagers")
					BLT.Mods._updates_notification = BLT.Notifications:add_notification({
						title = managers.localization:text("blt_checking_updates"),
						text = managers.localization:text("blt_checking_updates_help"),
						icon = icon,
						icon_texture_rect = rect,
						color = Color.white,
						priority = 1000
					})
				end
				count = count + 1
				update:CheckForUpdates(callback(self, self, "clbk_got_update"))
			end
		end
	end
	if (count == 0) then
		if not BLT.Mods._updates_notification then
			-- Place a notification that we dont have updateable mods
			local icon, rect = tweak_data.hud_icons:get_icon_data("csb_pagers")
			BLT.Mods._updates_notification = BLT.Notifications:add_notification({
				title = managers.localization:text("blt_checking_updates_none_required"),
				text = managers.localization:text("blt_checking_updates_none_required_help"),
				icon = icon,
				icon_texture_rect = rect,
				color = Color.white,
				priority = 1000
			})
		end
	end
end

------------------
if JimHUD:getSetting({ "SkipIt", "DISABLE_AUTO_UPDATES" }, false) then

	-- suppress auto updates at start
	BLT.Mods._has_checked_for_updates = true
	BLT.Mods._has_really_checked_for_updates = false

	local init_managers_original = MenuSetup.init_managers
	local mouse_pressed_original = BLTNotificationsGui.mouse_pressed

	function MenuSetup:init_managers(...)
		init_managers_original(self, ...)
		-- create initial notification
		if BLT.Mods._has_checked_for_updates then
			local icon, rect = tweak_data.hud_icons:get_icon_data("csb_pagers")
			BLT.Mods._updates_notification = BLT.Notifications:add_notification({
				title = "Welcome!",
				text = "Right-click here to check for updates.",
				icon = icon,
				icon_texture_rect = rect,
				color = Color.white,
				priority = 0,
			})
		end
	end

	function BLTNotificationsGui:mouse_pressed(button, x, y, ...)
		if not self._enabled then
			return
		end
		if (not BLT.Mods._has_really_checked_for_updates) and button == Idstring("1") and alive(self._content_panel) and self._content_panel:inside(x, y) then
			BLT.Mods._has_really_checked_for_updates = true
			-- trigger manual update check
			if BLT.Mods._updates_notification then
				BLT.Notifications:remove_notification(BLT.Mods._updates_notification)
				BLT.Mods._updates_notification = nil
			end
			call_on_next_update(callback(BLT.Mods, BLT.Mods, "_RunAutoCheckForUpdates"))
			return true
		else
			return mouse_pressed_original(self, button, x, y, ...)
		end
	end

end
