if string.lower(RequiredScript) == "lib/tweak_data/weaponfactorytweakdata" then
    local WeaponFactoryTweakData_init_sights_original = WeaponFactoryTweakData._init_sights
    local WeaponFactoryTweakData_init_content_dlc1_original = WeaponFactoryTweakData._init_content_dlc1
    local WeaponFactoryTweakData_init_content_dlc2_dec16_original = WeaponFactoryTweakData._init_content_dlc2_dec16
    local WeaponFactoryTweakData_init_content_jobs_original = WeaponFactoryTweakData._init_content_jobs

    function WeaponFactoryTweakData:_init_sights()
        WeaponFactoryTweakData_init_sights_original(self)

        -- Lebensauger .308
        if JimHUD:getSetting({ "Fixes", "SightFixes", "Lebensauger" }, true) then
            self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_wa2000 = { translation = Vector3(-0.045, -10, 0.75) } -- Sight
            self.parts.wpn_fps_upg_o_45iron.stance_mod.wpn_fps_snp_wa2000 = { translation = Vector3(-2.44, 0, -8.98), rotation = Rotation(0, 0, -45) } -- Angled Sight
            self.parts.wpn_fps_upg_o_shortdot.stance_mod.wpn_fps_snp_wa2000 = { translation = Vector3(-0.035, -28, -0.022) } -- Scope
            self.parts.wpn_fps_upg_o_leupold.stance_mod.wpn_fps_snp_wa2000 = { translation = Vector3(-0.035, -27, -0.122) } -- Theia
        end

        -- Reinfeld
        if JimHUD:getSetting({ "Fixes", "SightFixes", "Reinfeld" }, true) then
            self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_aimpoint.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_aimpoint_2.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_docter.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_eotech.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_t1micro.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) }
        end

        -- Locomotive
        if JimHUD:getSetting({ "Fixes", "SightFixes", "Locomotive" }, true) then
            self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_aimpoint.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_aimpoint_2.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_docter.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_eotech.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_t1micro.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) }
        end

    end

    function WeaponFactoryTweakData:_init_content_dlc1()
        WeaponFactoryTweakData_init_content_dlc1_original(self)

        -- Reinfeld
        if JimHUD:getSetting({ "Fixes", "SightFixes", "Reinfeld" }, true) then
            self.parts.wpn_fps_upg_o_cmore.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) } -- Reinfeld
        end

        -- Locomotive
        if JimHUD:getSetting({ "Fixes", "SightFixes", "Locomotive" }, true) then
            self.parts.wpn_fps_upg_o_cmore.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) } -- Locomotive
        end
    end

    function WeaponFactoryTweakData:_init_content_dlc2_dec16()
        WeaponFactoryTweakData_init_content_dlc2_dec16_original(self)

        -- Reinfeld
        if JimHUD:getSetting({ "Fixes", "SightFixes", "Reinfeld" }, true) then
            self.parts.wpn_fps_upg_o_acog.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) } -- Reinfeld
        end

        -- Locomotive
        if JimHUD:getSetting({ "Fixes", "SightFixes", "Locomotive" }, true) then
            self.parts.wpn_fps_upg_o_acog.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) } -- Locomotive
        end

    end

    function WeaponFactoryTweakData:_init_content_jobs(...)
        WeaponFactoryTweakData_init_content_jobs_original(self, ...)

        -- Reinfeld
        if JimHUD:getSetting({ "Fixes", "SightFixes", "Reinfeld" }, true) then
            self.parts.wpn_fps_upg_o_eotech_xps.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_reflex.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_rx01.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_rx30.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_cs.stance_mod.wpn_fps_shot_r870 = { translation = Vector3(0, 0, -3.2) }
        end

        -- Locomotive
        if JimHUD:getSetting({ "Fixes", "SightFixes", "Locomotive" }, true) then
            self.parts.wpn_fps_upg_o_eotech_xps.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_reflex.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_rx01.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_rx30.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) }
            self.parts.wpn_fps_upg_o_cs.stance_mod.wpn_fps_shot_serbu = { translation = Vector3(0, 0, -3.2) }
        end

    end

elseif string.lower(RequiredScript) == "lib/tweak_data/playertweakdata" then
    local PlayerTweakData_init_new_stances_original = PlayerTweakData._init_new_stances
    local PlayerTweakData_init_serbu_original = PlayerTweakData._init_serbu
    local PlayerTweakData_init_m32_original = PlayerTweakData._init_m32

    function PlayerTweakData:_init_new_stances(...)
        PlayerTweakData_init_new_stances_original(self, ...)

        -- Reinfeld
        if JimHUD:getSetting({ "Fixes", "SightFixes", "Reinfeld" }, true) then
            self.stances.r870 = deep_clone(self.stances.default)
            local pivot_shoulder_translation = Vector3(10.7362, 12.88858, -4.29568)
            local pivot_shoulder_rotation = Rotation(0.106618, -0.0844415, 0.629205)
            local pivot_head_translation = Vector3(7.5, 12, -3)
            local pivot_head_rotation = Rotation(0, 0, 0)
            self.stances.r870.standard.shoulders.translation = pivot_head_translation - pivot_shoulder_translation:rotate_with(pivot_shoulder_rotation:inverse()):rotate_with(pivot_head_rotation)
            self.stances.r870.standard.shoulders.rotation = pivot_head_rotation * pivot_shoulder_rotation:inverse()
            self.stances.r870.standard.vel_overshot.pivot = pivot_shoulder_translation + Vector3(0, -30, 0)
            self.stances.r870.standard.vel_overshot.yaw_neg = -5
            self.stances.r870.standard.vel_overshot.yaw_pos = 3
            self.stances.r870.standard.vel_overshot.pitch_neg = 2
            self.stances.r870.standard.vel_overshot.pitch_pos = -3
            local pivot_head_translation = Vector3(0, 15, 0)
            local pivot_head_rotation = Rotation(0, 0, 0)
            self.stances.r870.steelsight.shoulders.translation = pivot_head_translation - pivot_shoulder_translation:rotate_with(pivot_shoulder_rotation:inverse()):rotate_with(pivot_head_rotation)
            self.stances.r870.steelsight.shoulders.rotation = pivot_head_rotation * pivot_shoulder_rotation:inverse()
            self.stances.r870.steelsight.vel_overshot.pivot = pivot_shoulder_translation + Vector3(0, -30, 0)
            self.stances.r870.steelsight.vel_overshot.yaw_neg = -3
            self.stances.r870.steelsight.vel_overshot.yaw_pos = 3
            self.stances.r870.steelsight.vel_overshot.pitch_neg = 2
            self.stances.r870.steelsight.vel_overshot.pitch_pos = -2
            local pivot_head_translation = Vector3(6.5, 11, -4)
            local pivot_head_rotation = Rotation(0, 0, 0)
            self.stances.r870.crouched.shoulders.translation = pivot_head_translation - pivot_shoulder_translation:rotate_with(pivot_shoulder_rotation:inverse()):rotate_with(pivot_head_rotation)
            self.stances.r870.crouched.shoulders.rotation = pivot_head_rotation * pivot_shoulder_rotation:inverse()
            self.stances.r870.crouched.vel_overshot.pivot = pivot_shoulder_translation + Vector3(0, -30, 0)
        end

    end

    function PlayerTweakData:_init_serbu(...)
        PlayerTweakData_init_serbu_original(self, ...)

        -- Locomotive 12G
        if JimHUD:getSetting({ "Fixes", "SightFixes", "Locomotive" }, true) then
            self.stances.serbu = deep_clone(self.stances.default)
            local pivot_shoulder_translation = Vector3(10.71, 18.7035, -4.30218)
            local pivot_shoulder_rotation = Rotation(0.106838, -0.0844692, 0.629273)
            local pivot_head_translation = Vector3(7, 15, -3.5)
            local pivot_head_rotation = Rotation(0, 0, 0)
            self.stances.serbu.standard.shoulders.translation = pivot_head_translation - pivot_shoulder_translation:rotate_with(pivot_shoulder_rotation:inverse()):rotate_with(pivot_head_rotation)
            self.stances.serbu.standard.shoulders.rotation = pivot_head_rotation * pivot_shoulder_rotation:inverse()
            self.stances.serbu.standard.vel_overshot.pivot = pivot_shoulder_translation + Vector3(0, -20, 0)
            self.stances.serbu.standard.vel_overshot.yaw_neg = 6
            self.stances.serbu.standard.vel_overshot.yaw_pos = -6
            self.stances.serbu.standard.vel_overshot.pitch_neg = -4
            self.stances.serbu.standard.vel_overshot.pitch_pos = 4
            local pivot_head_translation = Vector3(0, 24, 0)
            local pivot_head_rotation = Rotation(0, 0, 0)
            self.stances.serbu.steelsight.shoulders.translation = pivot_head_translation - pivot_shoulder_translation:rotate_with(pivot_shoulder_rotation:inverse()):rotate_with(pivot_head_rotation)
            self.stances.serbu.steelsight.shoulders.rotation = pivot_head_rotation * pivot_shoulder_rotation:inverse()
            self.stances.serbu.steelsight.vel_overshot.pivot = pivot_shoulder_translation + Vector3(0, -10, 0)
            self.stances.serbu.steelsight.vel_overshot.yaw_neg = 5
            self.stances.serbu.steelsight.vel_overshot.yaw_pos = -4
            self.stances.serbu.steelsight.vel_overshot.pitch_neg = -5
            self.stances.serbu.steelsight.vel_overshot.pitch_pos = 5
            local pivot_head_translation = Vector3(6, 17, -4)
            local pivot_head_rotation = Rotation(0, 0, 0)
            self.stances.serbu.crouched.shoulders.translation = pivot_head_translation - pivot_shoulder_translation:rotate_with(pivot_shoulder_rotation:inverse()):rotate_with(pivot_head_rotation)
            self.stances.serbu.crouched.shoulders.rotation = pivot_head_rotation * pivot_shoulder_rotation:inverse()
            self.stances.serbu.crouched.vel_overshot.pivot = pivot_shoulder_translation + Vector3(0, -20, 0)
        end

    end

    function PlayerTweakData:_init_m32(...)
        PlayerTweakData_init_m32_original(self, ...)

        -- M32
        if JimHUD:getSetting({ "Fixes", "SightFixes", "M32" }, true) then
            self.stances.m32 = deep_clone(self.stances.default)
            local pivot_shoulder_translation = Vector3(9.71578, 20.864, -3.15926)
            local pivot_shoulder_rotation = Rotation(-3.7146E-6, 0.00110462, 3.00785E-4)
            local pivot_head_translation = Vector3(9, 27.5, -2.5)
            local pivot_head_rotation = Rotation(0, 0, 0)
            self.stances.m32.standard.shoulders.translation = pivot_head_translation - pivot_shoulder_translation:rotate_with(pivot_shoulder_rotation:inverse()):rotate_with(pivot_head_rotation)
            self.stances.m32.standard.shoulders.rotation = pivot_head_rotation * pivot_shoulder_rotation:inverse()
            self.stances.m32.standard.vel_overshot.pivot = pivot_shoulder_translation + Vector3(0, -40, 0)
            self.stances.m32.standard.vel_overshot.yaw_neg = 10
            self.stances.m32.standard.vel_overshot.yaw_pos = -10
            self.stances.m32.standard.vel_overshot.pitch_neg = -10
            self.stances.m32.standard.vel_overshot.pitch_pos = 10
            local pivot_head_translation = Vector3(0, 13, 2.35)
            local pivot_head_rotation = Rotation(0, 5, 0)
            self.stances.m32.steelsight.shoulders.translation = pivot_head_translation - pivot_shoulder_translation:rotate_with(pivot_shoulder_rotation:inverse()):rotate_with(pivot_head_rotation)
            self.stances.m32.steelsight.shoulders.rotation = pivot_head_rotation * pivot_shoulder_rotation:inverse()
            self.stances.m32.steelsight.vel_overshot.pivot = pivot_shoulder_translation + Vector3(0, -40, 0)
            self.stances.m32.steelsight.vel_overshot.yaw_neg = 10
            self.stances.m32.steelsight.vel_overshot.yaw_pos = -10
            self.stances.m32.steelsight.vel_overshot.pitch_neg = -10
            self.stances.m32.steelsight.vel_overshot.pitch_pos = 10
            local pivot_head_translation = Vector3(7.5, 21, -3.5)
            local pivot_head_rotation = Rotation(0, 0, 0)
            self.stances.m32.crouched.shoulders.translation = pivot_head_translation - pivot_shoulder_translation:rotate_with(pivot_shoulder_rotation:inverse()):rotate_with(pivot_head_rotation)
            self.stances.m32.crouched.shoulders.rotation = pivot_head_rotation * pivot_shoulder_rotation:inverse()
            self.stances.m32.crouched.vel_overshot.pivot = pivot_shoulder_translation + Vector3(0, -40, 0)
        end

    end

end
