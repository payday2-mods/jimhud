local CrimeSpreeTweakData_init_original = CrimeSpreeTweakData.init
function CrimeSpreeTweakData:init(...)
    CrimeSpreeTweakData_init_original(self, ...)

    if JimHUD:getSetting({ "Fixes", "CRIMESPREE_CRASH" }, true) then
        -- detect crashdump, not older than 10 minutes
        local t = 10
        local d = os.date("!*t")
        local str = tostring(d.year) .. '_' .. tostring(d.month) .. '_' .. tostring(d.day) .. 'T(%d%d?)_(%d%d?)'
        local files = SystemFS:list('', false) or {}
        for i = #files, 1, -1 do
            local h, m = files[i]:match(str)
            h = h and tonumber(h) or nil
            m = m and tonumber(m) or nil
            if m and h and ((h == d.hour and d.min - m < t) or (h + 1 == d.hour and d.min + 60 - m < t)) then
                -- disable crimespree loss for this session
                self.crash_causes_loss = false
            end
        end
    end
end
