local function expect_yes(self, orig_func, params, ...)
	if JimHUD:getSetting({ "Fixes", "NO_CONFIRM_DIALOGS" }, true) and params.yes_func then
		params.yes_func()
	else
		return orig_func(self, params, ...)
	end
end

-- Skills

local show_confirm_skillpoints_original = MenuManager.show_confirm_skillpoints
function MenuManager:show_confirm_skillpoints(params, ...)
	expect_yes(self, show_confirm_skillpoints_original, params, ...)
end

local show_confirm_respec_skilltree_original = MenuManager.show_confirm_respec_skilltree
function MenuManager:show_confirm_respec_skilltree(params, ...)
	expect_yes(self, show_confirm_respec_skilltree_original, params, ...)
end

-- Offshore contracts

local show_confirm_buy_premium_contract_original = MenuManager.show_confirm_buy_premium_contract
function MenuManager:show_confirm_buy_premium_contract(params, ...)
	expect_yes(self, show_confirm_buy_premium_contract_original, params, ...)
end

-- Weapons

local show_confirm_blackmarket_buy_original = MenuManager.show_confirm_blackmarket_buy
function MenuManager:show_confirm_blackmarket_buy(params, ...)
	expect_yes(self, show_confirm_blackmarket_buy_original, params, ...)
end

local show_confirm_blackmarket_sell_original = MenuManager.show_confirm_blackmarket_sell
function MenuManager:show_confirm_blackmarket_sell(params, ...)
	expect_yes(self, show_confirm_blackmarket_sell_original, params, ...)
end

local show_confirm_blackmarket_mod_original = MenuManager.show_confirm_blackmarket_mod
function MenuManager:show_confirm_blackmarket_mod(params, ...)
	expect_yes(self, show_confirm_blackmarket_mod_original, params, ...)
end

-- Slots

local show_confirm_blackmarket_buy_mask_slot_original = MenuManager.show_confirm_blackmarket_buy_mask_slot
function MenuManager:show_confirm_blackmarket_buy_mask_slot(params, ...)
	expect_yes(self, show_confirm_blackmarket_buy_mask_slot_original, params, ...)
end

local show_confirm_blackmarket_buy_weapon_slot_original = MenuManager.show_confirm_blackmarket_buy_weapon_slot
function MenuManager:show_confirm_blackmarket_buy_weapon_slot(params, ...)
	expect_yes(self, show_confirm_blackmarket_buy_weapon_slot_original, params, ...)
end

-- Masks

local show_confirm_blackmarket_mask_sell_original = MenuManager.show_confirm_blackmarket_mask_sell
function MenuManager:show_confirm_blackmarket_mask_sell(params, ...)
	expect_yes(self, show_confirm_blackmarket_mask_sell_original, params, ...)
end

local show_confirm_blackmarket_mask_remove_original = MenuManager.show_confirm_blackmarket_mask_remove
function MenuManager:show_confirm_blackmarket_mask_remove(params, ...)
	expect_yes(self, show_confirm_blackmarket_mask_remove_original, params, ...)
end

local show_confirm_blackmarket_finalize_original = MenuManager.show_confirm_blackmarket_finalize
function MenuManager:show_confirm_blackmarket_finalize(params, ...)
	expect_yes(self, show_confirm_blackmarket_finalize_original, params, ...)
end

local show_confirm_blackmarket_assemble_original = MenuManager.show_confirm_blackmarket_assemble
function MenuManager:show_confirm_blackmarket_assemble(params, ...)
	expect_yes(self, show_confirm_blackmarket_assemble_original, params, ...)
end

-- Assets

local show_confirm_mission_asset_buy_original = MenuManager.show_confirm_mission_asset_buy
function MenuManager:show_confirm_mission_asset_buy(params, ...)
	expect_yes(self, show_confirm_mission_asset_buy_original, params, ...)
end

local show_confirm_mission_asset_buy_all_original = MenuManager.show_confirm_mission_asset_buy_all
function MenuManager:show_confirm_mission_asset_buy_all(params, ...)
	expect_yes(self, show_confirm_mission_asset_buy_all_original, params, ...)
end

-- Infamy

local show_confirm_become_infamous_original = MenuManager.show_confirm_become_infamous
function MenuManager:show_confirm_become_infamous(params, ...)
	expect_yes(self, show_confirm_become_infamous_original, params, ...)
end

local show_confirm_infamypoints_original = MenuManager.show_confirm_infamypoints
function MenuManager:show_confirm_infamypoints(params, ...)
	expect_yes(self, show_confirm_infamypoints_original, params, ...)
end
