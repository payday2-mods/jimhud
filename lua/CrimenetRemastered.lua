if string.lower(RequiredScript) == "lib/tweak_data/guitweakdata" then

	local init_original = GuiTweakData.init

	function GuiTweakData:init()
		init_original(self)
		if JimHUD:getSetting({ "CRIMENET", "HIDE_BORDERS" }, true) then
			self.crime_net.regions = {}
		end
	end

elseif string.lower(RequiredScript) == "lib/managers/crimenetmanager" then

	CrimeNetGui.GRID_SAFE_RECT = { 1750, 900 }

	local init_original = CrimeNetGui.init
	local _create_locations_original = CrimeNetGui._create_locations
	local _get_job_location_original = CrimeNetGui._get_job_location
	local _create_job_gui_original = CrimeNetGui._create_job_gui
	local update_server_job_original = CrimeNetGui.update_server_job
	local update_job_gui_original = CrimeNetGui.update_job_gui

	CrimeNetGui.COLUMNS = Global.SKIP_OVERKILL_290 and 6 or 7

	function CrimeNetGui:init(...)
		-- settings
		self._grid_align = JimHUD:getSetting({ "CRIMENET", "GRID_ALIGN" }, true)
		self._sort_by_difficulty = JimHUD:getSetting({ "CRIMENET", "SORT_BY_DIFFICULTY" }, true)
		self._colorize_by_difficulty = JimHUD:getSetting({ "CRIMENET", "COLORIZE_BY_DIFFICULTY" }, true)
		self._replace_skulls = JimHUD:getSetting({ "CRIMENET", "REPLACE_SKULLS" }, true)
		self._white_pixel_fix = JimHUD:getSetting({ "CRIMENET", "WHITE_PIXEL_FIX" }, true)
		self._reduce_glow = JimHUD:getSetting({ "CRIMENET", "REDUCE_GLOW" }, true)
		self._job_scale = JimHUD:getSetting({ "CRIMENET", "JOB_SCALE" }, 0.7)
		self._rows = 10
		self._margins = { 113, 152, 0, 0 }
		init_original(self, ...)
	end

	-- callback, which gets called when creating the crimenet grid with random locations
	function CrimeNetGui:_create_locations(...)
		_create_locations_original(self, ...)

		-- replace grid for custom sorting - originally taken from PocoHUD, but optimized.
		if self._grid_align then
			local newDots = {}
			for i = 1, CrimeNetGui.COLUMNS do
				for j = 1, self._rows do
					local newX = self._margins[1] + (CrimeNetGui.GRID_SAFE_RECT[1] - self._margins[1] - self._margins[3]) * i / CrimeNetGui.COLUMNS
					local newY = self._margins[2] + (CrimeNetGui.GRID_SAFE_RECT[2] - self._margins[2] - self._margins[4]) * ((i % 2 == 0) and j or (j - 0.5)) / self._rows
					table.insert(newDots, { math.round(newX), math.round(newY) })
				end
			end
			self._locations[1][1].dots = newDots -- replace backend data of _get_contact_locations()
		end
	end

	-- callback, which gets called when reserving a position on the grid while creating a new job entry
	function CrimeNetGui:_get_job_location(data, ...)
		local x, y, d = _get_job_location_original(self, data, ...)

		-- sort by difficulty - originally taken from PocoHUD, but optimized.
		if self._sort_by_difficulty then
			local diff = (data and data.difficulty_id or 2) - 1
			local diffX = self._margins[1] + (CrimeNetGui.GRID_SAFE_RECT[1] - self._margins[1] - self._margins[3]) * diff / CrimeNetGui.COLUMNS
			local locations = self:_get_contact_locations() -- get next position from self._locations
			local sorted = {}
			for _, dot in pairs(locations[1].dots) do
				if not dot[3] then
					table.insert(sorted, dot)
				end
			end
			if #sorted > 0 then
				local abs = math.abs
				table.sort(sorted, function(a, b)
					return abs(diffX - a[1]) < abs(diffX - b[1])
				end)
				d = sorted[1]
				x, y = d[1], d[2]
				local map = self._map_panel:child("map")
				local tw = math.max(map:texture_width(), 1)
				local th = math.max(map:texture_height(), 1)
				x = math.round(x / tw * self._map_size_w)
				y = math.round(y / th * self._map_size_h)
			end
		end

		return x, y, d
	end

	function CrimeNetGui:jimhud_colorize_by_difficulty(obj, diff)
		---@diagnostic disable-next-line: undefined-field
		obj:set_color(math.lerp(JimHUD:getColorSetting({ "CRIMENET", "DIFFICULTY_COLOR_START" }, "white"), JimHUD:getColorSetting({ "CRIMENET", "DIFFICULTY_COLOR_END" }, "orange"), math.clamp((diff - 1) / (CrimeNetGui.COLUMNS - 1), 0, 1)))
	end

	-- create job entry
	function CrimeNetGui:_create_job_gui(data, type, fixed_x, fixed_y, fixed_location, ...)

		-- hack scaling (text size)
		local size = tweak_data.menu.pd2_small_font_size -- keep original scaling
		tweak_data.menu.pd2_small_font_size = size * self._job_scale -- apply temp scaling
		local result = _create_job_gui_original(self, data, type, fixed_x, fixed_y, fixed_location, ...)
		tweak_data.menu.pd2_small_font_size = size -- revert temp scaling

		if result.side_panel then
			-- apply scaling (icon size)
			local job_plan_icon = result.side_panel:child("job_plan_icon")
			local host_name = result.side_panel:child("host_name")
			if job_plan_icon and host_name then
				job_plan_icon:set_size(job_plan_icon:w() * self._job_scale, job_plan_icon:h() * self._job_scale)
				host_name:set_position(job_plan_icon:right() + 2, 0) -- fix align
			end
			-- colorize by difficulty
			local job_name = result.side_panel:child("job_name")
			if self._colorize_by_difficulty and job_name and not data.mutators and not data.is_crime_spree and type ~= "crime_spree" then
				CrimeNetGui:jimhud_colorize_by_difficulty(job_name, (data.difficulty_id or 2) - 1)
			end
		end
		-- reduce glow
		if result.heat_glow and self._reduce_glow then
			result.heat_glow:set_alpha(result.heat_glow:alpha() * 0.5)
		end

		return result
	end

	-- update job entry
	function CrimeNetGui:update_server_job(data, i, ...)
		update_server_job_original(self, data, i, ...)

		-- get job data
		local job_index = data.id or i
		local job = self._jobs[job_index]

		-- colorize by difficulty
		if job.side_panel and self._colorize_by_difficulty and not data.mutators and not data.is_crime_spree then
			CrimeNetGui:jimhud_colorize_by_difficulty(job.side_panel:child("job_name"), (data.difficulty_id or 2) - 1)
		end
	end

	-- fix hidden texts creating white pixels
	function CrimeNetGui:update_job_gui(job, inside, ...)
		update_job_gui_original(self, job, inside, ...)
		if self._white_pixel_fix and job.jimhud_mouse_over ~= inside then
			job.jimhud_mouse_over = inside
			job.side_panel:animate(self.jimhud_animate_white_pixel_fix, inside == 1 and 1 or 0)
		end

		if self._replace_skulls then
			job.side_panel:child("stars_panel"):clear()
			if job.job_id then
				local x = 0
				local y = 0
				local difficulty_stars = job.difficulty_id - 2
				local start_difficulty = 1
				local num_difficulties = Global.SKIP_OVERKILL_290 and 5 or 6
				for i = start_difficulty, num_difficulties do
					job.side_panel:child("stars_panel"):bitmap({
						texture = "guis/textures/pd2/hud_difficultymarkers_2",
						h = 17,
						w = 18,
						layer = 0,
						x = x - 3,
						y = y - 3,
						rotation = 360,
						texture_rect = {
							i == 4 and 90 or i == 6 and 30 or 0,
							i >= 5 and 32 or 0,
							30,
							30
						},
						alpha = difficulty_stars < i and 0.5 or 1,
						blend_mode = difficulty_stars < i and "normal" or "add",
						color = difficulty_stars < i and Color.black or tweak_data.screen_colors.risk
					})
					x = x + 12
				end
			end
		end

	end

	function CrimeNetGui.jimhud_animate_white_pixel_fix(panel, wanted_alpha)
		local contact_name = panel:child("contact_name")
		local info_name = panel:child("info_name")
		local difficulty_name = panel:child("difficulty_name")
		local heat_name = panel:child("heat_name")
		local one_down_label = panel:child("one_down_label")

		local current_alpha = info_name:alpha()
		local alpha_met = false
		local dt = nil
		while not alpha_met do
			dt = coroutine.yield()
			---@diagnostic disable-next-line: undefined-field
			current_alpha = math.step(current_alpha, wanted_alpha, dt * 2)
			contact_name:set_alpha(current_alpha)
			info_name:set_alpha(current_alpha)
			difficulty_name:set_alpha(current_alpha)
			heat_name:set_alpha(current_alpha)
			alpha_met = contact_name:alpha() == wanted_alpha and info_name:alpha() == wanted_alpha and difficulty_name:alpha() == wanted_alpha and heat_name:alpha() == wanted_alpha
			if one_down_label then
				one_down_label:set_alpha(current_alpha)
				alpha_met = alpha_met and one_down_label:alpha() == wanted_alpha
			end
		end
	end

elseif string.lower(RequiredScript) == "lib/managers/menu/items/contractbrokerheistitem" then

	local function make_fine_text(text)
		local _, _, w, h = text:text_rect()
		text:set_size(w, h)
		text:set_position(math.round(text:x()), math.round(text:y()))
		return w, h
	end

	function ContractBrokerHeistItem:init(parent_panel, job_data, idx, ...)

		-- set fields
		self._parent = parent_panel
		self._job_data = job_data

		-- get job info
		local job_tweak = tweak_data.narrative:job_data(job_data.job_id)
		local contact = job_tweak.contact
		local contact_tweak = tweak_data.narrative.contacts[contact]
		local dlc_name_text, dlc_color = self:get_dlc_name_and_color(job_tweak)
		local is_dlc = ((dlc_name_text ~= nil) and (dlc_name_text ~= ""))

		-- get settings
		local hide_image = JimHUD:getSetting({ "CRIMENET", "BROKER_HIDE_IMAGE" }, true)
		local hide_contact = JimHUD:getSetting({ "CRIMENET", "BROKER_HIDE_CONTACT" }, false)
		local hide_dlc = (not is_dlc) or JimHUD:getSetting({ "CRIMENET", "BROKER_HIDE_DLC_TAG" }, false)
		local hide_new = (not job_data.is_new) or JimHUD:getSetting({ "CRIMENET", "BROKER_HIDE_NEW_TAG" }, true)
		local show_heat = JimHUD:getSetting({ "CRIMENET", "SHOW_CONTRACTOR_JOB_HEAT" }, true)

		local line_height = JimHUD:getSetting({ "CRIMENET", "BROKER_LINE_HEIGHT" }, 22)
		local line_padding = JimHUD:getSetting({ "CRIMENET", "BROKER_LINE_PADDING" }, 1)

		-- enforce max font size values and see if we can fit in two lines accordingly
		local one_line_font_size_max = 22
		local two_line_font_size_max = 48
		local content_height = line_height - (line_padding * 2)
		local font_size = math.min(content_height * 0.8, one_line_font_size_max)
		local two_lines = false
		if content_height >= (one_line_font_size_max * 2) + line_padding then -- usable space is bigger than two lines would be, so..
			two_lines = true -- use two lines
			font_size = math.min(content_height * 0.8 / 2, two_line_font_size_max) -- enforce max font size
		end
		-- pick font by size: small <= one_line_font_size_max > large
		local font = (font_size > one_line_font_size_max) and tweak_data.menu.pd2_large_font or tweak_data.menu.pd2_small_font

		-- get text padding from line padding, but enforce min value
		local text_padding_min = 3
		local text_padding = math.max(line_padding, text_padding_min)

		-- create panel
		self._panel = parent_panel:panel({
			halign = "grow",
			layer = 10,
			valign = "top",
			x = 0,
			y = line_height * (idx - 1),
			h = line_height
		})

		-- create background
		self._background = self._panel:rect({
			blend_mode = "add",
			alpha = 0.4,
			halign = "grow",
			layer = -1,
			valign = "grow",
			y = line_padding,
			h = content_height,
			color = job_data.enabled and tweak_data.screen_colors.button_stage_3 or tweak_data.screen_colors.important_1,
			visible = false,
		})

		-- create image panel
		self._image_panel = self._panel:panel({
			halign = "left",
			layer = 1,
			valign = "center",
			x = 0,
			y = line_padding,
			w = not hide_image and content_height * 1.7777777777777777 or 0,
			h = content_height,
			visible = not hide_image,
		})
		-- try get image
		local has_image = false
		if job_tweak.contract_visuals and job_tweak.contract_visuals.preview_image then
			local data = job_tweak.contract_visuals.preview_image
			local path, rect = nil, nil
			if data.id then
				path = "guis/dlcs/" .. (data.folder or "bro") .. "/textures/pd2/crimenet/" .. data.id
				rect = data.rect
			elseif data.icon then
				path, rect = tweak_data.hud_icons:get_icon_data(data.icon)
			end
			if path and DB:has(Idstring("texture"), path) then
				self._image_panel:bitmap({
					valign = "scale",
					layer = 2,
					blend_mode = "add",
					halign = "scale",
					texture = path,
					texture_rect = rect,
					w = self._image_panel:w(),
					h = self._image_panel:h(),
					color = Color.white
				})
				self._image = self._image_panel:rect({
					alpha = 1,
					layer = 1,
					color = Color.black
				})
				has_image = true
			end
		end
		-- fallback image
		if not has_image then
			local color = Color.red
			local error_message = "Missing Preview Image"
			self._image_panel:rect({
				alpha = 0.4,
				layer = 1,
				color = color
			})
			self._image_panel:text({
				vertical = "center",
				wrap = true,
				align = "center",
				word_wrap = true,
				layer = 2,
				text = error_message,
				font = font,
				font_size = font_size
			})
			BoxGuiObject:new(self._image_panel:panel({ layer = 100 }), { sides = { 1, 1, 1, 1 } })
		end

		-- create job name
		local job_name = self._panel:text({
			layer = 1,
			vertical = "center",
			align = "left",
			halign = "left",
			valign = "top",
			text = managers.localization:to_upper_text(job_tweak.name_id),
			font = font,
			font_size = font_size,
			color = job_data.enabled and tweak_data.screen_colors.text or tweak_data.screen_colors.important_1
		})
		make_fine_text(job_name)
		-- x: right to image + padding
		job_name:set_left(((not hide_image) and self._image_panel:right() or 0) + text_padding)
		if two_lines and not hide_contact then -- two lines and contact visible?
			job_name:set_top(self._image_panel:center_y()) -- y: below contact
		else
			job_name:set_center_y(self._image_panel:center_y()) -- y: center
		end

		-- create contact name
		local contact_name = self._panel:text({
			alpha = two_lines and 0.8 or 0.7,
			layer = 1,
			vertical = "center",
			align = "left",
			halign = "left",
			valign = "top",
			text = not hide_contact and managers.localization:to_upper_text(contact_tweak.name_id) or "",
			font = font,
			font_size = font_size * (two_lines and 0.9 or 0.7),
			color = tweak_data.screen_colors.text,
			visible = not hide_contact
		})
		make_fine_text(contact_name)
		if two_lines and not hide_contact then -- two lines and visible?
			contact_name:set_left(job_name:left()) -- x: align with job name
			contact_name:set_bottom(self._image_panel:center_y()) -- y: above job name
		else
			contact_name:set_left(job_name:right() + text_padding) -- x: right to job name + padding
			contact_name:set_center_y(self._image_panel:center_y()) -- y: center
		end

		-- create dlc tag
		local dlc_name
		if not hide_dlc then
			dlc_name = self._panel:text({
				alpha = 1,
				vertical = "top",
				layer = 1,
				align = "left",
				halign = "left",
				valign = "top",
				text = dlc_name_text,
				font = font,
				font_size = font_size * (two_lines and 0.9 or 0.7),
				color = dlc_color
			})
			make_fine_text(dlc_name)
			if not hide_contact then
				dlc_name:set_left(contact_name:right() + text_padding) -- x: right to job name + padding
			else
				dlc_name:set_left(job_name:right() + text_padding) -- x: right to job name + padding
			end
			if two_lines then
				dlc_name:set_bottom(job_name:top()) -- y: above job name
			else
				dlc_name:set_center_y(job_name:center_y()) -- y: center
			end
		end

		-- create new tag
		local new_name
		if not hide_new then
			new_name = self._panel:text({
				alpha = 1,
				vertical = "top",
				layer = 1,
				align = "left",
				halign = "left",
				valign = "top",
				text = managers.localization:to_upper_text("menu_new"),
				font = font,
				font_size = font_size * (two_lines and 0.9 or 0.7),
				color = Color(255, 105, 254, 59) / 255
			})
			make_fine_text(new_name)
			if not hide_dlc then -- dlc visible?
				new_name:set_left(dlc_name:right() + text_padding) -- x: right to dlc + padding
			elseif not hide_contact then -- contact visible?
				new_name:set_left(contact_name:right() + text_padding) -- x: right to contact name + padding
			else
				new_name:set_left(job_name:right() + text_padding) -- x: right to job name + padding
			end
			if two_lines then -- two lines?
				new_name:set_bottom(job_name:top()) -- y: above job name
			else
				new_name:set_center_y(job_name:center_y()) -- y: center
			end
		end

		local heat_name
		if show_heat and job_tweak and job_tweak.contact ~= "skirmish" then
			local heat_text, heat_color = self:get_job_heat_text(self._job_data.job_id)
			heat_name = self._panel:text({
				alpha = 1,
				vertical = "top",
				layer = 1,
				align = "left",
				halign = "left",
				valign = "top",
				text = heat_text,
				font = font,
				font_size = font_size * (two_lines and 0.9 or 0.7),
				color = heat_color
			})
			make_fine_text(heat_name)
			if not hide_new then -- new visible?
				heat_name:set_left(new_name:right() + text_padding) -- x: right to new + padding
			elseif not hide_dlc then -- dlc visible?
				heat_name:set_left(dlc_name:right() + text_padding) -- x: right to dlc + padding
			elseif not hide_contact then -- contact visible?
				heat_name:set_left(contact_name:right() + text_padding) -- x: right to contact name + padding
			else
				heat_name:set_left(job_name:right() + text_padding) -- x: right to job name + padding
			end
			if two_lines then -- two lines?
				heat_name:set_bottom(job_name:top()) -- y: above job name
			else
				heat_name:set_center_y(job_name:center_y()) -- y: center
			end
		end

		-- create icon panel
		local icons_panel = self._panel:panel({
			valign = "top",
			halign = "right",
			h = line_height,
			w = self._panel:w(),
		})
		icons_panel:set_right(self._panel:w())

		local last_icon = nil

		-- fav icon
		local icon_size = font_size * 1.1
		self._favourite = icons_panel:bitmap({
			texture = "guis/dlcs/bro/textures/pd2/favourite",
			vertical = "top",
			align = "right",
			halign = "right",
			alpha = 0.8,
			valign = "top",
			color = Color.white,
			w = icon_size,
			h = icon_size,
		})
		self._favourite:set_right(icons_panel:w() - text_padding) -- x: align right
		self._favourite:set_center_y(job_name:center_y()) -- y: center
		last_icon = self._favourite

		-- day icon
		local day_text = icons_panel:text({
			layer = 1,
			vertical = "center",
			align = "right",
			halign = "right",
			valign = "center",
			text = self:get_heist_day_text(),
			font = font,
			font_size = font_size * 0.9,
			color = tweak_data.screen_colors.text
		})
		make_fine_text(day_text)
		day_text:set_right(last_icon:left() - text_padding) -- x: left to last icon
		day_text:set_center_y(job_name:center_y()) -- y: center
		last_icon = day_text

		-- len icon
		local length_icon = icons_panel:text({
			layer = 1,
			vertical = "center",
			align = "right",
			halign = "right",
			valign = "center",
			text = self:get_heist_day_icon(),
			font = font,
			font_size = font_size * 0.7,
			color = tweak_data.screen_colors.text
		})
		make_fine_text(length_icon)
		length_icon:set_right(last_icon:left() - text_padding) -- x: left to last icon
		length_icon:set_center_y(job_name:center_y()) -- y: center
		last_icon = length_icon

		-- stealth icon
		if self:is_stealthable() then
			local stealth = icons_panel:text({
				layer = 1,
				vertical = "center",
				align = "right",
				halign = "right",
				valign = "center",
				text = managers.localization:get_default_macro("BTN_GHOST"),
				font = font,
				font_size = font_size,
				color = tweak_data.screen_colors.text
			})
			make_fine_text(stealth)
			stealth:set_right(last_icon:left() - text_padding) -- x: left to last icon
			stealth:set_center_y(job_name:center_y()) -- y: center
			last_icon = stealth
		end

		-- last played icon
		local last_played = self._panel:text({
			alpha = 0.7,
			vertical = "center",
			layer = 1,
			align = "right",
			halign = "right",
			valign = "center",
			text = self:get_last_played_text(),
			font = font,
			font_size = font_size * (two_lines and 0.8 or 0.7),
			color = tweak_data.screen_colors.text
		})
		make_fine_text(last_played)
		if two_lines then
			last_played:set_right(self._panel:w() - text_padding)
			last_played:set_bottom(job_name:top())
		else
			last_played:set_right(last_icon:left() - text_padding * 2)
			last_played:set_center_y(job_name:center_y())
		end

		-- refresh
		self:refresh()
	end

	function ContractBrokerHeistItem:get_job_heat_text(job_id)
		local heat_text      = ""
		local heat_color     = Color(1, 0, 1)
		local exp_multiplier = managers.job:heat_to_experience_multiplier(managers.job:get_job_heat(job_id) or 0)
		local exp_percent    = ((1 - exp_multiplier) * -1) * 100

		if exp_percent ~= 0 then
			local prefix = exp_percent > 0 and "+" or ""
			heat_text = prefix .. exp_percent .. "%"
			heat_color = exp_percent > 0 and tweak_data.screen_colors.heat_warm_color or tweak_data.screen_colors.heat_cold_color
		end

		return heat_text, heat_color
	end

end
