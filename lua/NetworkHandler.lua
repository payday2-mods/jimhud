--do return end	-- Disabled cause: WiP
if JimHUD and not JimHUD.Sync then
	JimHUD.Sync = {
		msg_id = "JimHUD_Sync",
		peers = { false, false, false, false },
		events = {
			discover_jimhud       = "Using_JimHUD?",
			confirm_jimhud        = "Using_JimHUD!",
			peer_disconnect       = "Leaving_Game",
			locked_assault_status = "locked_assault_status",
		},
	}

	local Net = _G.LuaNetworking

	function JimHUD.Sync.table_to_string(tbl)
		return Net:TableToString(tbl) or ""
	end

	function JimHUD.Sync.string_to_table(str)
		local tbl = Net:StringToTable(str) or ""

		for k, v in pairs(tbl) do
			tbl[k] = JimHUD.Sync.to_original_type(v)
		end

		return tbl
	end

	function JimHUD.Sync.to_original_type(s)
		local v = s
		if type(s) == "string" then
			if s == "nil" then
				v = nil
			elseif s == "true" or s == "false" then
				v = (s == "true")
			else
				v = tonumber(s) or s
			end
		end
		return v
	end

	function JimHUD.Sync:send_to_peer(peer_id, messageType, data)
		if peer_id and peer_id ~= Net:LocalPeerID() and messageType then
			local tags = {
				id = self.msg_id,
				event = messageType
			}

			if type(data) == "table" then
				data = self.table_to_string(data)
				tags["table"] = true
			end

			Net:SendToPeer(peer_id, self.table_to_string(tags), data or "")
		end
	end

	function JimHUD.Sync:send_to_host(messageType, data)
		self:send_to_peer(managers.network:session():server_peer():id(), messageType, data)
	end

	function JimHUD.Sync:send_to_all_peers(messageType, data)
		for peer_id, enabled in ipairs(self.peers) do
			self:send_to_peer(peer_id, messageType, data)
		end
	end

	function JimHUD.Sync:send_to_all_discovered_peers(messageType, data)
		for peer_id, enabled in ipairs(self.peers) do
			if enabled then
				self:send_to_peer(peer_id, messageType, data)
			end
		end
	end

	function JimHUD.Sync:send_to_all_undiscovered_peers(messageType, data)
		for peer_id, enabled in ipairs(self.peers) do
			if not enabled then
				self:send_to_peer(peer_id, messageType, data)
			end
		end
	end

	function JimHUD.Sync:receive_message(peer_id, event, data)
		if peer_id and event then
			local events = JimHUD.Sync.events

			if event == events.discover_jimhud then
				JimHUD.Sync:send_to_peer(peer_id, events.confirm_jimhud)
				JimHUD.Sync.peers[peer_id] = true
				managers.chat:feed_system_message(ChatManager.GAME, "Client " .. tostring(peer_id) .. " is using JimHUD ;)") --TEST
			elseif event == events.confirm_jimhud then
				JimHUD.Sync.peers[peer_id] = true
				managers.chat:feed_system_message(ChatManager.GAME, "The Host is using JimHUD ;)") --TEST
			elseif event == events.peer_disconnect then
				JimHUD.Sync.peers[peer_id] = false
			elseif event == events.locked_assault_status then
				managers.hud:_locked_assault(data)
			end
		end
	end

	-- Manage Networking and list of peers to sync to...
	Hooks:Add("NetworkReceivedData", "NetworkReceivedData_JimHUDSync", function(sender, messageType, data)
		sender = tonumber(sender)
		if sender then
			local tags = JimHUD.Sync.string_to_table(messageType)
			if JimHUD.Sync and tags.id and tags.id == JimHUD.Sync.msg_id and not string.is_nil_or_empty(tags.event) then
				if tags.table then
					data = JimHUD.Sync.string_to_table(data)
				else
					data = JimHUD.Sync.to_original_type(data)
				end

				JimHUD.Sync:receive_message(sender, tags.event, data)
			end
		end
	end)

	Hooks:Add("BaseNetworkSessionOnPeerRemoved", "BaseNetworkSessionOnPeerRemoved_JimHUDSync", function(self, peer, peer_id, reason)
		JimHUD.Sync:receive_message(peer_id, JimHUD.Sync.events.peer_disconnect, "")
	end)

	Hooks:Add("BaseNetworkSessionOnLoadComplete", "BaseNetworkSessionOnLoadComplete_JimHUDSync", function(local_peer, id)
		if JimHUD.Sync and Net:IsMultiplayer() and Network:is_client() then
			JimHUD.Sync:send_to_host(JimHUD.Sync.events.discover_jimhud)
		end
	end)


	-- Data Sync functions:

	function JimHUD.Sync:endless_assault_status(status)
		if Network:is_server() then
			self:send_to_all_discovered_peers(JimHUD.Sync.events.locked_assault_status, tostring(status))
		end
	end
end
