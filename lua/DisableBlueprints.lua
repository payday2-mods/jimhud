local BlackMarketTweakData_init_weapon_skins_original = BlackMarketTweakData._init_weapon_skins
function BlackMarketTweakData:_init_weapon_skins(...)
    BlackMarketTweakData_init_weapon_skins_original(self, ...)

    if JimHUD:getSetting({ "INVENTORY", "DISABLE_SKIN_MODS" }, true) then
        for _, weap in pairs(self.weapon_skins) do
            if weap.rarity ~= "legendary" and weap.default_blueprint then
                weap.default_blueprint = nil
            end
        end
    end
end
