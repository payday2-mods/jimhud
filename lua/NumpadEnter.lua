local function table_pack(...)
    return { n = select("#", ...), ... }
end

local numpadenter = Idstring("num enter")
local normalenter = Idstring("enter")

local function NumpadEnterConfirm_RemapNumpadEnter(...)
    local args = table_pack(...)
    for i = 1, args.n do
        if args[i] == numpadenter then
            args[i] = normalenter
            break
        end
    end
    return args
end

if string.lower(RequiredScript) == "lib/managers/hud/hudchat" then
    local HUDChat_key_release_original = HUDChat.key_release
    local HUDChat_key_press_original = HUDChat.key_press

    function HUDChat:key_release(...)
        HUDChat_key_release_original(self, unpack(NumpadEnterConfirm_RemapNumpadEnter(...)))
    end

    function HUDChat:key_press(...)
        HUDChat_key_press_original(self, unpack(NumpadEnterConfirm_RemapNumpadEnter(...)))
    end

elseif string.lower(RequiredScript) == "lib/managers/chatmanager" then
    local ChatGui_key_release_original = ChatGui.key_release
    local ChatGui_key_press_original = ChatGui.key_press

    function ChatGui:key_release(...)
        ChatGui_key_release_original(self, unpack(NumpadEnterConfirm_RemapNumpadEnter(...)))
    end

    function ChatGui:key_press(...)
        ChatGui_key_press_original(self, unpack(NumpadEnterConfirm_RemapNumpadEnter(...)))
    end

elseif string.lower(RequiredScript) == "core/lib/managers/controller/corecontrollerwrapperpc" then
    core:module("CoreControllerWrapperPC")
    core:import("CoreControllerWrapper")
    local ControllerWrapperPC_virtual_connect_confirm_original = ControllerWrapperPC.virtual_connect_confirm

    function ControllerWrapperPC:virtual_connect_confirm(...)
        ControllerWrapperPC_virtual_connect_confirm_original(self, ...)

        local args = table_pack(...)
        args[3] = "num enter"
        self:virtual_connect2(unpack(args))
    end

end
