if string.lower(RequiredScript) == "lib/managers/crimenetmanager" then
	local init_original = CrimeNetGui.init
	local mouse_pressed_original = CrimeNetGui.mouse_pressed
	local mouse_moved_original = CrimeNetGui.mouse_moved

	function CrimeNetGui:init(ws, fullscreeen_ws, node, ...)
		init_original(self, ws, fullscreeen_ws, node, ...)
		self._reconnect_enabled = JimHUD:getSetting({ "MENU_TWEAKS", "CRIMENET", "RECONNECT" }, true)
		if self._reconnect_enabled then
			local key = BLT.Keybinds:get_keybind("jimhud_keybind_Reconnect"):Key() or "insert"
			local reconnect_button = self._panel:text({
				name = "reconnect_button",
				text = string.upper("[" .. key .. "]" .. " Reconnect"),
				font_size = tweak_data.menu.pd2_small_font_size,
				font = tweak_data.menu.pd2_small_font,
				color = tweak_data.screen_colors.button_stage_3,
				layer = 40,
				y = 40,
				blend_mode = "add"
			})
			self:make_fine_text(reconnect_button)
			reconnect_button:set_right(self._panel:w() - 10)
			self._fullscreen_ws:connect_keyboard(Input:keyboard())
			self._fullscreen_panel:key_press(callback(self, self, "KeyPressed"))
		end
	end

	function CrimeNetGui:KeyPressed(o, k)
		if self._reconnect_enabled then
			local key = BLT.Keybinds:get_keybind("jimhud_keybind_Reconnect"):Key() or "insert"
			if k == Idstring(key) and alive(self._panel:child("reconnect_button")) then
				JimHUD:Reconnect()
			end
		end
	end

	function CrimeNetGui:mouse_pressed(o, button, x, y, ...)
		if not self._crimenet_enabled or self._getting_hacked then
			return
		end
		if self._reconnect_enabled then
			local reconnect_button = self._panel:child("reconnect_button")
			if alive(reconnect_button) and reconnect_button:inside(x, y) then
				JimHUD:Reconnect()
				return true
			end
		end
		return mouse_pressed_original(self, o, button, x, y, ...)
	end

	function CrimeNetGui:mouse_moved(o, x, y, ...)
		mouse_moved_original(self, o, x, y, ...)
		if not self._crimenet_enabled or self._getting_hacked then
			return
		end
		if self._reconnect_enabled then
			local reconnect_button = self._panel:child("reconnect_button")
			if alive(reconnect_button) then
				if reconnect_button:inside(x, y) then
					if not self._reconnect_highlighted then
						self._reconnect_highlighted = true
						reconnect_button:set_color(tweak_data.screen_colors.button_stage_2)
						managers.menu_component:post_event("highlight")
					end
				elseif self._reconnect_highlighted then
					self._reconnect_highlighted = false
					reconnect_button:set_color(tweak_data.screen_colors.button_stage_3)
				end
			end
		end
	end

elseif string.lower(RequiredScript) == "lib/network/matchmaking/networkmatchmakingsteam" then
	local join_server_original = NetworkMatchMakingSTEAM.join_server

	function NetworkMatchMakingSTEAM:join_server(room_id, ...)
		join_server_original(self, room_id, ...)
		if room_id then
			JimHUD:SaveLastRoomID(room_id)
			JimHUD:print_log("Saving STEAM room ID " .. room_id, "info")
		end
	end

elseif string.lower(RequiredScript) == "lib/network/matchmaking/networkmatchmakingepic" then
	local join_server_original = NetworkMatchMakingEPIC.join_server

	function NetworkMatchMakingEPIC:join_server(room_id, ...)
		join_server_original(self, room_id, ...)
		if room_id then
			JimHUD:SaveLastRoomID(room_id)
			JimHUD:print_log("Saving EPIC room ID " .. room_id, "info")
		end
	end

end
