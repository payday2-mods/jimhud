# JimHUD

## Description

This is a Mod collection of several useful HUD altering mods, based on Kamikaze94's [WolfHUD](https://github.com/Kamikaze94/WolfHUD).

## Requirements

To make this mod work you will need to install __[SuperBLT](https://superblt.znix.xyz)__.

## Installation

1. Click on the **Clone or Download** Button at the top of this site
2. Click on the **Download .ZIP** Button
3. Open the downloaded archive, using WinRAR or 7Zip
4. Extract the **jimhud-master** folder into your 'PAYDAY 2/mods' folder
5. Start the game.

## Bug reports

If you encounter any bugs, feel free to post about it on the [issues](https://gitlab.com/payday2-mods/jimhud/-/issues) tab!
Please try to provide information from your crashlog and the actions you and your teammates were doing, when you crashed. ;)
The Crashlog can be found at **%localappdata%/PAYDAY 2/crashlog.txt**.
The BLT log can be found at **PAYDAY 2/mods/logs/**.

## Changes & additional features & integrated mods (compared to WolfHUD)

* Based on WolfHUD - commit stage: https://github.com/Kamikaze94/WolfHUD/tree/1b2736f9290062fe73ef3f2f10b913255def4905
* Changed default values
* Updated advanced assault indicator
* Reworked config menus
* Hide dlc advertisements
* Disable automatic update check in main menu (right click to search manually)
* Disable catching of bags by bots
* Disable crimespree loss on restarts, 10 minutes after a crash
* Remove Vanilla U217 Downcounter
* Auto decline telemetry agreement (idea by **Dr_Newbie**)
* Create empty lobby (idea by **Snh20**)
* Custom waypoints color (idea by **kju8**)
* Side jobs and steam items in main menu & lobbies (based on Restructured menus by **TdlQ**)
* Crime.net remastered (partially based on PocoHud by **Zenyr**)
* Scrollable Menus (by **Luffy**)
* Remove Confirmation Dialogs (based on code by **ReaperTeh**, **notwa**)
* Custom Objective Animation (originally from the pd2 beta, taken from an early version of Restoration Mod)
* Stale Lobby Contract Fix (by **Snh20**)
* Shoot through AI (by **Martini**, **Elysium**)
* Sight Fixes for Lebensauger, Locomotive, Reinfeld (by **Malo** and **TdlQ**) and M32 (by **Cloudy_Jay**)
* Disabled skin included modifications (by **>:3** / **Pawcio1337**)
* Numpad Enter For Confirm (by **Some name here**)
* Improved Main Menu Info Panel (blatantly stolen from HopHUD, by **Hoppip**)
* [Floating Health Bars](https://modworkshop.net/mod/20330) (partially based on PocoHud by **Zenyr**)
* [Straight To Main Menu](https://modworkshop.net/mod/26946) (by **Snh20**)
* [Fixed armor sorting](https://modworkshop.net/mod/13585) (by **EdisLeado**)
* [Reconnect to server](https://modworkshop.net/mod/13546) (by **Luffy**)
* [No Idle Intro](https://modworkshop.net/mod/19097) (by **Jindetta**)
* [Instant Cardflip](https://modworkshop.net/mod/20839) (by **Takku**)

### Merged from VanillaHUDPlus

* HPS Meter
* Armor cd timer
* Inspire Indicator
* Underdog Indicator
* Bulletstorm Indicator
* Fixed kill counter/tabstats/bufflist etc.
* Updated Laser Gadget compatibility
* A few other minor fixes here and there

## WolfHUD-based included features/mods

* [CustomHUD](https://bitbucket.org/pjal3urb/customhud/src) made by **Seven** modified by **Kamikaze94**
* [HudPanelList](https://bitbucket.org/pjal3urb/hudlist/src/) made by **Seven**, modified by **Kamikaze94**
* [KillCounter](https://bitbucket.org/pjal3urb/customhud/src) made by **Seven**, modified by **Kamikaze94**
* [Accuracy Plugin](https://bitbucket.org/pjal3urb/customhud/src) made by **Seven**, modified by **Kamikaze94**
* [ToggleInteract](https://bitbucket.org/pjal3urb/toggleinteract/src) made by **Seven**, modified by **Iron Predator** and **Kamikaze94**
* [DoubleTap Granades](https://bitbucket.org/pjal3urb/doubletapgrenades/src) made by **Seven**
* [AutoPickup](https://bitbucket.org/pjal3urb/autopickup/src) made by **Seven**
* [WeaponGadgets](https://bitbucket.org/pjal3urb/gadgets) made by **Seven**
* [Remember Gadget State](https://bitbucket.org/pjal3urb/persistentgadgets/src) made by **Seven**
* [TabStats](https://steamcommunity.com/app/218620/discussions/15/618463738399320805/) made by **friendIyfire**
* [Numeric Suspicion](https://github.com/cjur3/GageHud) made by **friendIyfire**, updated by **Kamikaze94**
* Angeled Sight Visualisation, made by **Kamikaze94** (based on HoxHUD P4.1)
* [EnemyHealthbar](https://modworkshop.net/mydownloads.php?action=view_down&did=15157) made by **Undeadsewer**, modified by **Kamikaze94**
* AdvAssault made by **Kamikaze94** (based on HoxHUD P4.1)
* Dynamic Damage Indicator made by **Kamikaze94**
* DamagePopups made by **Kamikaze94** (Uses [WaypointsManager](https://bitbucket.org/pjal3urb/waypoints) made by **Seven**)
* [BurstFire](https://bitbucket.org/pjal3urb/burstfire/src) made by **Seven**
* [Real Ammo](https://modworkshop.net/mydownloads.php?action=view_down&did=15108) made by **FishTaco**, modified by **Kamikaze94**
* [No Spam](http://steamcommunity.com/app/218620/discussions/15/618457398976607330/) made by **Ahab**, **Seven**, **AmEyeBlind** & **money123451**, updated by **Kamikaze94**
* [DrivingHUD](https://modworkshop.net/mydownloads.php?action=view_down&did=12982) made by **ViciousWalrus** and **Great Big Bushy Beard**, rewritten by **Kamikaze94**
* [Real Weapon Names](http://modworkshop.net/mydownloads.php?action=view_down&did=15433) made by **(AD) Jaqueto**, updated by **Terminator01**
* [Buy all Assets](http://steamcommunity.com/app/218620/discussions/15/618458030689719683/) rewritten by **Kamikaze94**
* PrePlanManager made by **Kamikaze94**
* ProfileMenu made by **Kamikaze94**
* Equipment Tweaks made by **Kamikaze94**
* Smaller Menu Tweaks
  * [Always show Mod Icons](http://modworkshop.net/mydownloads.php?action=view_down&did=13975) made by **Slippai**, updated by **Kamikaze94**
  * [Slider Values](http://modworkshop.net/mydownloads.php?action=view_down&did=14800) made by **Snh20**, modified by **Kamikaze94**
  * Show Weapon Names made by **Kamikaze94**
  * Show Skill Names made by **Kamikaze94**
  * [Skillset Info](http://modworkshop.net/mydownloads.php?action=view_down&did=15294) made by **Fooksie**
  * Increased the maximum chars of custom Weapon/Mask/Skillset names
  * ~~Federal Inventory (Mod Override), made by **Nervatel Hanging Closet Monster** and **Luffy**, updated by **Kamikaze94**~~
    * ~~[Weapon Icons](https://modworkshop.net/mydownloads.php?action=view_down&did=14240), [Melee Icons](http://modworkshop.net/mydownloads.php?action=view_down&did=13910) and [Mask Icons](http://modworkshop.net/mydownloads.php?action=view_down&did=13911) made by **Nervatel Hanging Closet Monster**~~
    * ~~[Throwables, Equipment, Armor and Heister Portraits](http://modworkshop.net/mydownloads.php?action=view_down&did=13916) made by **Luffy**~~

## Localizations

* English made by **Kamikaze94**
* German made by **Kamikaze94**
* Russian made by **chrom[K]a**, **Magic3000** & **MEXAHOTABOP**
* Korean made by **Я!zu**
* Spanish made by **papydeath95** & **ElReyZero1201**
* Chinese made by **zhongfly** & **CoolLKK**
* French made by **Elise MRX (La Mule)**
* Portuguese made by **Kazenin (Aldo Raine)**
* Italian made by **LeecanIt**
* Dutch made by **Azoraqua**

Big credit goes to all of you!
Without your awesome mods, I would have quitted this game a long time ago!
**If I forgot to mention you, I'm really sorry.
Please feel free to contact me, so I can credit you, for the awesome stuff you have made :)**
